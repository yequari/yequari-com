let latest = document.querySelector('#last-update');
let avatar = document.querySelector('#avatar-frame');

let latestDate = Date.parse(latest.attributes.getNamedItem('datetime').value);
let threshold = new Date();
threshold.setDate(threshold.getDate() - 7);

console.log(latestDate)

if (latestDate < threshold) {
  avatar.className = 'busy';
} else {
  avatar.className = 'online';
}
