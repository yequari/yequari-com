---
title: "Fitness and Health"
date: 2024-04-02T15:00:28-07:00
---

# Fitness and Health

One of my goals for 2024 was to focus on my physical health. This included two sub-goals: a) exercise at the gym 3 times per week and b) weigh under 200lbs at the end of the year. This page is intended to track my progress.

Last Updated {{< param date >}}

## Body Weight

- Weighed on 01/04/2024: 227lbs
- Current Weight: 222lbs

I had a doctor appointment recently and the post-visit summary included obesity as a discussion topic, despite neither me or my doctor ever bringing it up. I don't know how realistic my weight goals in the given timeframe are, but as long as I'm trending downward at the end of the year, I'll be happy. 

## Lifts

- Squats: 115lbs
- Deadlift: 115lbs
- Bench Press: 90lbs
- Barbell Row: 90lbs
- Overhead Press: 65lbs

Currently, squats are my best lift, but I've stopped adding weight for now while I train up my shoulders, which are my weakest lift. Weightlifting is really enjoyable to me, it is a great feeling to become stronger.

## Cardio

God cardio is a struggle. I've been walking on the treadmill before doing my weight lifting. I tried running today but it was rough, I ended up on a stationary bike which felt a lot better, but I want to get good at running. 

## Dieting Rules

My intention with these rules is to be stricter with my diet and incentivize healthier choices, while avoiding detailed calorie counting and being miserable. I figure if I write them down it's easier to hold myself accountable rather than keeping only the vague idea of them in my mind. Most of my bad eating comes from sweets and sugary drinks, so I am reducing my intake of those. So far these rules have been working well for me, I am down a few pounds and also not miserable or craving junk food, which I will consider a win. These rules will be adjusted based on how things are going in a few months.   

- Chips and similar carb-heavy snack foods are allowed, but in a limited amount and eaten alongside a meal, never eaten alone.
- Dinner is the final food of the day, no snacking afterward
- Fast food (usually Taco Bell) is allowed once per week, but be mindful of caloric intake
- Zero-sugar sodas only
- Prefer zero-sugar beverages in general, but the occasional lemonade/juice/milkshake as a treat is ok
- Avoid caffeine after 3pm


## Focus Areas

- Improving my max weight on overhead press
- Improve cardio, ideally this means improving my mile time to under 10 mins
- Continue with dieting rules

