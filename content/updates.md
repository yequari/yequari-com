---
title: Changelog
---
### Changelog

**04/18/2023**: Update Mastodon link to reflect moving to a new instance.

**04/17/2023**: Regenerate website with Hugo. Remove Yesterweb webring widget following the closure of the webring

**03/22/2023**: Added more links to [links](/links) page.

**01/09/2023**: Change header logo, add status.cafe widget, update CSS responsiveness

**12/26/2022**: More backend upgrades. Revamped the [links page](/links). Added some IndieWeb support, most notable webmentions! Check out my post [I Uninstalled TikTok, Again](/blog/2022/11/goodbye-tiktok) to see some webmentions in action :)

**11/14/2022**: Major update to the backend side of things, now the site is (mostly) statically generated, which saves resources! Added some topic pages in tech, and landing pages for music and transformers, which will have content soon. I've also added an RSS feed.

**11/7/2022**: Minor style edits, style is much more responsive

**11/6/2022**: Added topic pages for shrines / things I want to talk about

**7/22/2022**: Moved navigation back to the top, made sidebar content unique per page

**7/6/2022**: Revamped links page

**6/24/2022**: Removed music player and Google web fonts, changed max-height, minor color changes. Updated /now

**5/7/2022**: Moved navigation from the top to the side, added an audio player, made stylesheet more responsive.
