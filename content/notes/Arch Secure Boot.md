---
title: Arch Linux Secure Boot
date: 2024-05-06T00:00:00-07:00
tags:
 - Linux
---
I needed to enable Secure Boot so that I could play League of Legends with Riot's Vanguard Anti Cheat. It seemed complex at first but was actually easier than I expected. The Arch Wiki is both helpful and confusing here, it provides a bunch of different ways to do this, but doesn't always do a great job of differentiating them as logically separate processes.

My basic understanding of Secure Boot is that when your computer first loads up, it loads the motherboard firmware, which then boots a bootloader. I remember from my OS classes that historically, the bootloader is 512 bytes, specifically the first 512 bytes on the boot disk. The bootloader can be anything, the computer will just run it because of its location in storage, which means malicious actors could run any arbitrary code there and compromise the system. Secure boot mitigates this by only allowing cryptographically signed bootloaders to run. The process outlined here is to sign our bootloaders so that Secure Boot will allow them to run. We are doing this by using our own keys (see [Assisted process with sbctl](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Assisted_process_with_sbctl)), rather than using a pre-signed boot loader.  Note that "firmware" here refers to the motherboard firmware, commonly referred to as "the BIOS"

0. Reboot into firmware and set secure boot mode to Setup mode. What this really means is clearing any keys that currently exist. 
1. Install `sbctl` and run `sbtcl status` to verify Setup mode is enabled
2. Create custom keys `sbctl create-keys`
3. Enroll your keys `sbctl enroll-keys -m`. the `-m` means to enroll Microsoft's keys as well. You almost always want this, even if you're not dual-booting with Windows because some firmware is still signed with Microsoft's keys
4. Check boot files that need signing `sbctl verify`
5. Sign the files `sbctl sign -s /boot/vmlinuz-linux`
    1. If you have a lot of files, the command given in the wiki works great `sbctl verify | sed 's/✗ /sbctl sign -s /e'`, this assumes that every file listed in `sbctl verify` starts with `/boot`
6. Make sure all files are signed `sbctl verify`
7. Reboot into firmware and turn on Secure Boot
    1. In my Asus ROG motherboard's firmware, the setting is in Boot -> Secure Boot and the options are "Windows UEFI" or "Other OS" which are unclear. Windows UEFI turns Secure Boot on. 
8. Reboot again and run `sbctl status` to verify Secure Boot is on
