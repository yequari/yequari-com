---
title: League on Linux
date: "2022-11-06T14:18:30-07:00"
tags:
 - Linux
---

# League of Legends on Linux

*LoLoL for short*

# Important update

With [Vanguard coming to League](/blog/2024/01/lol-anticheat) in the near future, League on Linux is no more.

These are my notes on how I got League of Legends running on my Linux system. It runs almost flawlessly, the only issues I have are with the client, but even on Windows the client is garbage. The game itself runs even better on Linux than it does Windows.

## System Info
OS: Kubuntu 22.04 LTS x86_64    
CPU: AMD Ryzen 5 5600X (12) @ 3.700GHz    
GPU: NVIDIA GeForce GTX 970
RAM: 8GB

## Credits

I learned how to do this from the community over at [r/leagueoflinux](https://old.reddit.com/r/leagueoflinux) so for any questions or issues, check over there. These are posts that were especially helpful to me.

- [Fixing the Nvidia 495 Black Screen Crash Problem](https://old.reddit.com/r/leagueoflinux/comments/skyg70/fixing_the_nvidia_495_black_screen_crash_problem/)
- [Decreased Client Lag No More Low-Spec Mode](https://www.reddit.com/r/leagueoflinux/comments/qyjv45/decreased_client_lag_no_more_low_spec_mode/)
- [r/leagueoflinux Wiki](https://old.reddit.com/r/leagueoflinux/wiki/index)

## 1. Install Lutris

### a. Install Lutris Client

```
sudo add-apt-repository ppa:lutris-team/lutris && sudo apt update && sudo apt install lutris
```

### b. Complete Prerequisites for League

Enable 32-bit architecture
```
sudo dpkg --add-architecture i386
```

Install the latest Nvidia and Vulkan drivers from the proprietary drivers PPA

```
sudo add-apt-repository ppa:graphics-drivers/ppa && sudo apt update && sudo apt install -y nvidia-driver-510 libvulkan1 libvulkan1:i386
```

Install Wine Dependencies

```
sudo apt update && sudo apt install -y wine64 wine32 libasound2-plugins:i386 libsdl2-2.0-0:i386 libdbus-1-3:i386 libsqlite3-0:i386
```

### c. Download League of Legends

https://lutris.net/games/league-of-legends/

Install standard version. 

Follow the instructions that Lutris presents. There are two downloads: the client and the game. ***IMPORTANT:*** Do NOT enter login details or click play. Simply close the laucher when downloads are complete.

## 2. Optimizations and Workarounds

### a. Delete and Disable DXVK cache
This prevents the game from getting stuck on a black screen when loading
1. Remove all files with `cxvk-cache` in their name from `~/Games/league-of-legends`
2. In Lutris, right click League of Legends -> Configure -> System Options -> Environment Variables, remove `DXVK_STATE_CACHE_PATH` and add `DXVK_STATE_CACHE=0`

### b. Add d3d Overrides
This improves client performance
1. In Lutris, League of Legends -> Wine configuration -> Libraries, add `d3d10 (native)` and `d3d11 (native)`

### c. Disable abi.vsyscall
```
sudo sysctl -w abi.vsyscall=0
```

This command needs to be run every time you boot your machine, or you will get an error when loading the game. There is a way to make this happen automatically, but I rarely fully shutdown my PC so it's not necessary for me.

### d. Black screen when loading into game

Sometimes when something goes wrong, the game will open a black screen and refuse to load. I'm not sure the cause of this but the only fix I've found is to restart your system.
