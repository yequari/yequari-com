---
title: Dockerized ZNC
date: "2022-11-07T14:18:30-07:00"
tags:
 - Linux
 - Self-Hosting
---

# Setting Up ZNC with Docker

ZNC is an IRC bouncer, which is a program that acts as a middleman between your IRC client and any servers you connect to. This provides a number of benefits, the most notable of which is chat history while your client is closed. This guide is how I set up 

I have a Raspberry Pi connected to my network that I use as a DNS server with [Pi-hole](https://pi-hole.net) and [nginx-proxy](https://hub.docker.com/r/nginxproxy/nginx-proxy), as well as a host for several web-based services. Each service is a Docker container, which I manage through a single Docker Compose file. To install ZNC, I simply add a container to the Compose file. 

```yaml
version: "3"

# https://github.com/pi-hole/docker-pi-hole/blob/master/README.md

services:
  nginx-proxy:
    image: nginxproxy/nginx-proxy
    ports:
      - '80:80'
    environment:
      DEFAULT_HOST: pihole.home.yequari.com
    volumes:
      - '/var/run/docker.sock:/tmp/docker.sock'
    restart: always

  pihole:
    image: pihole/pihole:latest
    ports:
      - '53:53/tcp'
      - '53:53/udp'
      - '8053:80/tcp'
    volumes:
      - './etc-pihole:/etc/pihole'
      - './etc-dnsmasq.d:/etc/dnsmasq.d'
    cap_add:
      - NET_ADMIN
    environment:
      ServerIP: 192.168.1.130
      PROXY_LOCATION: pihole
      VIRTUAL_HOST: pihole.home.yequari.com
      VIRTUAL_PORT: 80
    extra_hosts:
      # Resolve to nothing domains (terminate connection)
      # - 'nw2master.bioware.com nwn2.master.gamespy.com:0.0.0.0'
      # LAN hostnames for other docker containers using nginx-proxy
      - 'home.yequari.com:192.168.1.130'
      - 'pihole pihole.home.yequari.com:192.168.1.130'
      - 'jellyfin jellyfin.home.yequari.com:192.168.1.130'
      - 'freshrss freshrss.home.yequari.com:192.168.1.130'
      - 'znc znc.home.yequari.com:192.168.1.130'
    restart: always

  ## entries for other services omitted

  znc:
    image: lscr.io/linuxserver/znc:latest
    container_name: znc
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Phoenix
      - PROXY_LOCATION=znc
      - VIRTUAL_HOST=znc.home.yequari.com
      - VIRTUAL_PORT=6501
    volumes:
      - /mnt/hdisk1/znc:/config
    ports:
      - 6501:6501
      - 6697:6697
    restart: unless-stopped

```

The `VIRTUAL_HOST` domain name will redirect only to the web interface on port 6501. I also reserved port 6697 for IRC itself. While it is possible to reverse proxy the IRC port to use the domain name as well, it was more trouble that it was worth, so I just used the Raspberry Pi's local IP address to connect my IRC client. Though the official ZNC Docker image should work fine, I used the [Linuxserver image](https://hub.docker.com/r/linuxserver/znc), simply because I used their Jellyfin image previously due to its added support for Raspberry Pi systems. 

Once that was done, I logged into the web interface (default login is admin/admin) to create a new user and connect it to my preferred IRC network. These steps are pretty straightforward and well-documented, so I'll skip over them.

Now it's finally time to connect the IRC client to the ZNC server. I created a connection as normal, but used the ZNC server's IP address instead of the domain name. Double check port is correct (6697 in my case). Finally, I needed to provide a password of the format `<username>/<network>:<password>`, using the information I provided ZNC when setting up my user. For example `yequari/OFTC:supersecretpassword`. Note that `<network>` refers to the name you give the network in ZNC, not the URL. I was then able to connect and enjoy all the benefits of an IRC bouncer.
