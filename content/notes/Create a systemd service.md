---
title: Example Systemd Service
date: 2024-05-06T00:00:00-07:00
slug: systemd-example
tags:
 - Linux
---

System units go in `/etc/systemd/system`
User units go in `$HOME/.config/systemd/user`

```
[Unit]  
Description=Webring test service 
After=network.target
StartLimitIntervalSec=0

[Service]  
Type=simple
Restart=always
RestartSec=1
WorkingDir=/code/webring
ExecStart=/code/webring/webring --dsn /code/webring/webring.db --addr :8000
  
[Install]  
WantedBy=multi-user.target
```
