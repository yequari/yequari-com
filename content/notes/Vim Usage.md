---
title: Vim Tips
date: 2024-05-06T00:00:00-07:00
slug: vim-tips
tags:
 - Linux
---
## General Tips
- `vim -p file1 file2` to open multiple files as tabs
- `:qa` to quit out of all open buffers
- `:Ex` to return to netrw (file explorer)
## Terminals
- `:term` to open a terminal in current window
- Use `i` to enter insert mode and type into the shell
- `<C-\><C-n>` to return to normal mode to allow scrolling or switching windows
    - I've remapped this to `<C-space>`
## Windows
- `<C-w>` prefixes all window commands
- `<C-w>` + `h,j,k,l` to focus window to left, bottom, up, right, respectively
- `<C-w>s` to split current window horizontally, equivalent to `:split`
- `<C-w>v` to split current window vertically, equivalent to `:vs`
## Registers
- `"c` before a command, where `c` is the register to store the text
- Use capital letter to append to the register, e.g. `"C`
## Macros
- Press `qc` to start recording, where `c` is the register to store the macro
- Stop recording with `q`
- Replay the macro with `@c`
