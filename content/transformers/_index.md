---
title: My Transformers Collection
---
# Transformers 

I like to talk about Transformers and my collection of toys. My favorite character is Starscream and I unsurprisingly have a ton of his figures. 

## Figures I Like

- [Retro G1 Starscream (2022)](/transformers/vintage-starscream-2022/)
- [The Transfomers Collection #9 Starscream (2003)](/transformers/transformers-collection-starscream/)
