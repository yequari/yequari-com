---
title: "Now"
date: 2023-04-17T15:00:46-07:00
---
# Now

One day this page will pull some info from APIs to create a more interesting page but for now I just manually update it as needed.

**Games I'm playing**: Cyberpunk 2077, Honkai Star Rail, Baldur's Gate 3

**Shows I'm watching**: Nothing currently

**Music I'm listening to**: Kyuss, Tool, Cyberpunk 2077 Soundtrack, 100 gecs, King Crimson, breakcore
