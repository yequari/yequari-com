---
title: About
---
# About Me

Hello, I'm yequari. I like to play TCGs and TTRPGs. I've played a lot of D&D 5e but am currently exploring Pathfinder 2. I'm a software developer, interested in the smallweb and permacomputing, and trying to learn more about systems programming. I love to collect Transformers figures, my favorite is Starscream. 

If you'd like to get in contact with me, email me at yequari at 32bit dot cafe, or follow me on [Mastodon](https://retro.pizza/@yequari).
