---
title: "Projects"
date: 2023-10-09T14:21:12-07:00
---

Here are some projects I'm currently working on or have worked on in the past.

## 32-Bit Cafe

I run a few services for the amazing folks over at the [32-Bit Cafe](https://32bit.cafe).

  - [RSS reader](https://rss.32bit.cafe): an instance of the FreshRSS feed reader to provide an easy to use entry point for community members looking to get started with RSS.
  - ~~Discussion boards: An instance of postmill used to share interesting links and resources to the community.~~
  - ~~Discuss bot: A Discord bot that sends new posts from the discussion boards to a channel in the Discord server.~~
  - [32bit.cafe Discourse](https://discourse.32bit.cafe): The postmill instance has been phased out in favor of a Discourse forum. It has been awesome building this up with everyone and seeing the enthusiasm for a second primary space for the community.

## Websites

- This website
- [yequari's emporium of excellent toys](https://yeet.marigold.town): One day I will catalog my toy collection here
- [webweav.ing tools](https://webweav.ing): Currently there is only an RSS feed generator but I intend to add several other tools to help smooth out some pain points of webmastery.

## Software

- [Pynex](/projects/pynex): A terminal browser for m15o's nex protocol.
- [lox-interpreter](/projects/lox-interpreter): Source code as I'm working through Robert Nystrom's excellent *Crafting Interpeters*.
- [yqsh](/projects/yqsh): A toy shell written in C to learn Linux systems programming.
- [weatherbar](/projects/weatherbar): A cli application to get current weather conditions, for use in status bar programs like i3bar, waybar, etc. 
