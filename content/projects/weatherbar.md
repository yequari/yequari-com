---
title: "Weatherbar"
date: 2023-10-09T17:07:53-07:00
---

A cli utility that prints out the current weather. Designed to be used with Linux statusbar software such as i3bar, waybar, etc. 
