---
title: "Powerlinx"
date: 2023-10-09T17:07:18-07:00
---

Powerlinx is a static site generator that also packages the site as single executable. I originally wrote it to generate this website, which I wanted to have more dynamic content than what using Hugo would allow. A flawed concept, I've come to realize, as now I am back to using Hugo, but I learned a lot from writing this software and would like to revisit it, reworking it to be a simple HTML preprocessor. 
