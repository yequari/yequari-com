---
title: "Seekerbot"
date: 2023-10-09T17:07:11-07:00
---

One of my first side projects, which I created during the pandemic. At the time I was playing the Transformers TCG a lot, which had now moved online, and the communities I was playing in needed a way to track the result of matches. Since everything was on Discord, I realized I could put something together using the Discord API to track matches and win rates during a given period of time.

Slowly, interest in the fan continuation of the Transformers TCG dried up, so SeekerBot is not in use anymore.
