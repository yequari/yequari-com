---
title: "Pynex"
date: 2023-10-09T17:07:25-07:00
---

[Source Code](https://git.32bit.cafe/yequari/pynex/)

A terminal browser for m15o's [nex protocol](https://nightfall.city/nex/) written in Python.
