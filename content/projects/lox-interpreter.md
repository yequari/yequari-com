---
title: "Lox Interpreter"
date: 2023-10-09T17:07:41-07:00
---

While I was working on [yqsh](/projects/yqsh), I became curious how shells implement a scripting language, and fell down a rabbithole of learning about programming languages, compilers, and interpreters. This led me to find Robert Nystrom's [Crafting Interpreters](http://www.craftinginterpreters.com/), a wonderful book that guides the reader through creating a tree-walk interpreter and later a full bytecode virtual machine. I'm currently working through the first half of the book, the tree-walk interpreter in Java. 
