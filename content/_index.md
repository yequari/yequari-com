---
title: yequari.com
---
# Back at it again, for the very first time

Welcome to my homepage! I like to program and tinker with computers. My other hobbies include video games, tabletop RPGs, and collecting Transformers figures. On this site I like to write about the web, games, and random thoughts I have. I also publish my notes on various topics. 
