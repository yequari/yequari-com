---
title: "Building a Website is a Marathon, Not a Sprint"
date: 2024-05-06T16:58:43-07:00
year: "2024"
categories:
 - Internet
tags:
 - WeblogPoMo2024
---

When I first created my website just over two years ago, I spent a lot of time fretting over what exactly to put on it. The common answer of "anything you want!" wasn't helpful because I didn't actually know what I wanted. 

Spending time browsing others' personal sites and chatting with their webmasters gave me inspiration for new pages to build and new topics to blog about. After two years, I am learning what I want to put on my website. I enjoy blogging and sharing the things that I know and the things that I like. As a result, I've published more blog posts, created my links page, my /now page, and just yesterday, published a bunch of notes I've taken as I learn more about computers and web development. Coming soon will be a page to share music I've discovered recently. 

It took me a long time to get here, and that's okay. Personal websites are about expressing yourself, sometimes it takes times to figure out who you are and what you value, and even longer to properly express that. It is a long journey with no end goal, an infinite path with innumerable unforeseen turns. As I grow, my website grows alongside me.
