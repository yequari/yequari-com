---
title: "Happy (Belated) JS Naked Day"
date: 2024-04-26T17:12:11-07:00
year: "2024"
subtitle: Woah three blog posts in a week(ish) is unheard of
categories:
  - Internet
tags:
  - smallweb
  - web development
---

[JS Naked Day](https://js-naked-day.org/) happened this week, and as usual, I'm fashionably late. I decided to permanently remove a significant portion of the JavaScript from my site as it was affecting load times. There wasn't a whole lot of JavaScript to begin with, which made this pretty easy to implement. What I did have was the following:

## 1. A script to randomly select from an array of quotes to display in the sidebar on each page load

I just removed this as I just wasn't finding it amusing anymore. Maybe it will return later, who knows? Certainly not me.

## 2. A script to change the avatar frame color in the sidebar

I had originally implemented this using Hugo's templates, as I described in my post [Recreating the Windows Live Messenger Avatar in CSS](/blog/2024/02/windows-live-avatar-css), but opted for JavaScript instead because I wouldn't need to constantly rebuild my site. I had said rebuilding periodically was a waste of compute time, which was kind of bullshit because instead I was wasting the compute time of every visitor on every page load. So now I've gone back to the template approach, and build my website periodically every hour, unless I push out an update before then. Previously this was done with a bash script, but I rewrote it in python to separate the build step from the updating from git step so that I could do each at different intervals.

## 3. Status.cafe widget

I love status.cafe, it's a really easy way to get some dynamic content on your static website. The downside of this is that every page load is hitting m15o's servers twice: first to grab the linked JavaScript file and then to run it and grab a second json file with the actual status information. This took about 1.1s to complete from beginning to end, again, *on every page load*. To speed things up, I hacked together a partial template in Hugo that downloads the json file directly on build, using Hugo's [resources.GetRemote](https://gohugo.io/functions/resources/getremote/) function.

## 4. Webmentions.js

Webmentions.js is just a way to display webmentions that you've received. Which is pretty handy, except I receive almost zero webmentions. The ones I do receive often come from Mastodon via [Brid.gy](https://brid.gy/), which I've come to realize is [problematic from a privacy standpoint](https://brainbaking.com/post/2023/05/why-i-retired-my-webmention-server/). I'm going to remove my webmention support for a lot of the reasons listed in this post, which mirrors my experience with them. The places I get reactions to the stuff I write on my website are the places where I go and manually post links, usually Mastodon and the [32bit cafe](https://discourse.32bit.cafe), and it's just easier to reply to people on there. There's no real reason to syndicate those replies on my site. If I really care about adding webmention support again, I can probably find a way to do it during the website build rather than live via JavaScript.

## 5. tinylytics

This is the only JavaScript file I'm keeping. I really like the simplicity of tinylytics. I get to see what pages are most popular and an overall viewcount, and it's not at all intrusive. It loads behind the scenes on each page so it does not affect load times, and if you were to block JavaScript it does not lessen the user experience at all.

I'm not opposed to using JavaScript, but the cool stuff I was doing with it could be just as easily done with my static site generator. JavaScript may make a return to my website if I find something else cool to do with it, but for now I will just enjoy the super fast load times!
