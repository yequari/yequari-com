---
title: "February Updates"
date: 2024-02-04T17:18:05-07:00
year: "2024"
categories:
  - Internet
tags:
  - smallweb
  - 32-Bit Cafe
---

Man, my [one blog post per week](/blogs/2024/01/reflecting-on-2023) goal sure fell apart quickly. To avoid going too long without a post, I figure I'll give a quick update on what I've been working on. Over at the 32-Bit Cafe, we [announced we are expanding into a Discourse forum](https://discuss.32bit.cafe/f/32bitcafe/217/valentine-s-day-event-huge-announcement)! I've been spending a lot of time setting it up and working alongside the rest of the mod team to get it ready before the February 15th launch. This weekend, I was able to find some time to work on projects, though. I built a PC for my partner, which they have been enjoying! It was a much needed upgrade over their old system, which was a frankensteined build made out of the core of my first PC build from 2015. I also spent some time working through [Advanced Programming in the UNIX Environment](https://stevens.netmeister.org/631/). I'm working on the first homework assignment, which is a simple program that copies a file. It's been a fun challenge and I'm learning a lot. On the fitness side of things, I haven't made much progress yet. I actually took the week off from the gym because of a combination of sleep issues and hurting my back, but I'm feeling a lot better now and will be back at it tomorrow! While the first month of 2024 hasn't gone quite to plan, I've been keeping busy anyway.
