---
title: "Clearing My Game Backlog"
date: 2024-05-02T07:54:51-07:00
year: "2024"
categories:
  - Gaming
tags:
  - WeblogPoMo2024
---

I love video games. Or, at least, I think I do. I don't finish very many of them. I either play them for a few hours and forget to come back or I hyperfocus and get like 75% through the story but lose interest before getting to the end. This summer, I'd like to make progress on my backlog of unplayed games. I'll start with Baldur's Gate 3 and Cyberpunk 2077. These both fall into the 75% category and I last played them recently enough that I won't be completely lost with what's going on in the story. After that I'll dig into some indie games, stuff like Hades, Hyper Light Drifter, Hotline Miami 1 + 2, because they are shorter to complete and do a lot of interesting things. Then I get back into AAA games and beat one beginning to end, just to prove to myself that I can. I'm thinking one of Persona 5 Royal, Spider-Man, Ghost of Tsushima or Final Fantasy VII Remake. A challenge I face with this goal is that a lot of first-person and third-person games trigger my motion sickness, so I can only really play for an hour at a time until a build up a tolerance to them (which I lose if I don't play for a few days). Usually I stick to top-down games like strategy and simulation games to get around this, but I *really* want to play these games, so I will try my best to persevere. 
