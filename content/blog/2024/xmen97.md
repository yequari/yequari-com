---
title: "X-Men 97"
date: 2024-05-04T18:54:13-07:00
year: "2024"
categories:
 - Life
tags:
 - WeblogPoMo2024
---

I've been keeping up with X-Men 97 weekly, it's such a good show. I love that it has a pretty serious plot but also keeps some of that Saturday morning cartoon charm. The weekly release schedule is so nice, I love spending the week leading up to the next episode hypothesizing with my partner about what will happen next. It's also gotten me into the X-Men more in general, I've been watching the original 90s cartoon in between episodes, and I think I'll pick up some comics as well! 

