---
title: "Doing New Things Is Scary"
date: 2024-05-03T17:40:04-07:00
year: "2024"
categories:
 - Life
tags:
 - WeblogPoMo2024
---

I've been looking at gigs on Upwork to try and bring in some extra cash and bolster my experience. Some of these jobs seem braindead easy but I'm still so afraid to apply for them. Logically, I know that if something goes wrong nothing bad really happens, but the fact that something *could* go wrong terrifies me. I guess I just don't want to disappoint people. My browser currently has a half-filled out proposal for a job. It would take me a couple hours to complete. I just need to submit it. 
