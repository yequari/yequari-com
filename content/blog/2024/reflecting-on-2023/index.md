---
title: "Reflecting on 2023"
date: 2024-01-10T15:29:00-07:00
layout: winter
context: "This page is an entry into the 32-Bit Cafe's [2023 holiday event](https://32bit.cafe/holidays2023/)."
year: "2024"
categories:
  - Life
tags:
  - 32-Bit Cafe
---
![32-Bit Cafe Holiday Event 2023](32bit-cafe-holidays.png)

2023 was truly one of the years of all time. In this post, I will look back at some of the cool things I did in 2023 and set some goals to achieve in 2024. 
<!--more-->

## Accomplishments in 2023

In August I returned to the gym after 4ish years of not really exercising much at all. I've always had a difficult relationship with fitness and exercise. I've never really been fit and everytime I tried to change that, I set goals that were far too ambitious and then failed to meet them, at which point I would give up, because what's the point of doing something unless I can do it perfectly? This time, though, I've made it my goal to just get in the gym and exercise 3 times a week. It's been great so far. I've been pretty consistent with going, missing only two weeks, once when I was sick and once when I was out of town. I am starting to see some progress, though I haven't made any efforts to track it, I notice I am sleeping better and have more energy. I don't always feel like going to the gym but I am always glad I forced myself to go.

I also spent a lot of time hanging out at the [32-bit Cafe](https://32bit.cafe), working on my own website, helping others work on theirs, and just shooting the shit about web, tech, and life. It's been a great time, the friends I've made there 
I started a few web services for the cafe, an instance of [FreshRSS](https://rss.32bit.cafe) and an instance of [Postmill](https://discuss.32bit.cafe), as well as a Discord bot to feed new posts from the discussion boards to a channel in the Discord. 


Outside the cafe, I also started a few programming side projects. I started work on a [Linux shell](/projects/yqsh), which led me down a rabbithole of not only the Linux API but also scripting languages and how to design and implement them. As a result I started working through Robert Nystrom's [Crafting Interpreter](http://www.craftinginterpreters.com). It's been a lot of fun to learn all about how these tools that I use every day work on a deeper level, and I'm excited to continue learning about them. 

## Goals for 2024

Before I get into my goals, I'd like to talk a little bit about my philosophy regarding setting goals and measuring success. With how my brain is wired getting things done can be hard. I've spent many years setting goals, failing to meet them very early on, then feeling defeated and that continuing to work towards the goal is pointless. To get around that, I set goals that are more vague, but still have something quantifiable to measure success. The idea is to focus less on the result and more on the journey getting there. 

In 2024, I want to continue exercising consistently, and I think at this point I can set some goals and monitor my progress. As of January 4th, I weigh 227 pounds. I'd like to get that down to under 200 before the end of the year. I think it's doable, but not easy. Ideally, I can do this without counting calories because it's a major pain in the ass but if I need to, I will. 

I want to write more code in 2024 and further build my skills. I will finish my shell project, complete with a small scripting language. I want it to be usable, for myself and others. I don't expect anyone to actually download, install, and use it but it should be possible to do so regardless. I'm not sure how long this will take, but I'd like to make some amount of progress each week. I found this [Advanced Programming in the UNIX Environment](https://stevens.netmeister.org/631/) course to learn some of the more advanced systems programming concepts I will need to complete it, so the first few months of 2024 will be split between doing this coursework and working on my shell. After that, I'm going to focus on learning more operating systems concepts. I think this is the direction I want to take in my career so learning what I can about it seems like a good move. I'll dive into [Operating Systems: Three Easy Pieces](https://pages.cs.wisc.edu/~remzi/OSTEP/) once I'm ready.

In addition to working on side projects, I would like to start contributing to open source projects. This is something I have always been somewhat intimidated by but I think it is good practice for getting acquainted with large codebases and collaborating with larger teams. I will try to find a project that I use and is written in a language I'm comfortable in to get started hacking away. Right now I think [Neovim](https://neovim.io/) might be a good one to start with. I use it every day and it is written in C, which seems like a good fit for now. 

Finally, I am committed to updating my blog at least once a week. I've got some drafts started for posts I've wanted to publish for a while now, so now I will find time to complete them. I'm not being particular on length or topic, I just need to write *something* about a topic I find interesting each week. It will be challenging at first but my hope is that I will get better at it as the year goes on.

Overall, I had a pretty good year in 2023, and I'm looking forward to carrying that momentum forward into 2024. 
