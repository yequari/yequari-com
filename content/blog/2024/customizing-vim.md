---
title: "Customizing Vim"
date: 2024-05-07T16:22:42-07:00
year: "2024"
categories:
 - Tech
tags:
 - WeblogPoMo2024
---
I've been using vim (specifically neovim) for over a year, I really enjoy how customizable it is, though up until today, I've only customized it through plugins made by other people. Today I wrote two Lua functions to streamline my workflow. I'm not very familiar with Lua but it was quite easy to pick up.

First is a function to split my window so it would have two side by side, and a smaller one at the bottom, which gets turned into a terminal.

```lua
local createcodeenv = function ()
  vim.cmd('split')
  vim.cmd('wincmd j')
  local win = vim.api.nvim_get_current_win()
  local height = vim.api.nvim_win_get_height(win)
  vim.api.nvim_win_set_height(win, height - 15)
  vim.cmd('term')
  vim.cmd('wincmd k')
  vim.cmd('vsplit')
  win = vim.api.nvim_get_current_win()
  local buf = vim.api.nvim_get_current_buf()
  vim.api.nvim_win_set_buf(win, buf)
end
vim.api.nvim_create_user_command('CodeEnv', createcodeenv, {})
```

Second is a function to bind writing a file and returning to Netrw (the file explorer) to one command, :We.

```lua
local writeGoToNetrw = function()
  vim.cmd('w')
  vim.cmd('Ex')
end
vim.api.nvim_create_user_command('We', writeGoToNetrw, {})
```

Both of these were things I did manually basically every time I used vim. Defining custom commands to make them easier was a fun learning experience, I look forward to learning more about customizing vim.
