---
title: "Intermittent Fasting"
date: 2024-05-01T07:23:19-07:00
year: "2024"
categories:
  - life
tags:
  - WeblogPoMo2024
---

I've started doing intermittent fasting to help achieve my [fitness goals](/fitness). It's only been a week so far but I'm liking how it's making me be more intentional about my eating habits. My eating hours are from 11am to 7pm, so I basically get two full meals and some snacks. I thought the hard part would be waiting until 11am to eat, but it's actually pretty easy (probably all the coffee helping there), then when I do eat breakfast I can put more thought into it than when I'm half awake. What is actually hard is not being able to eat after 7pm. I eat dinner around 6 but by 9:30 or so I'm starting to get hungry again. Maybe the solution to this is just going to bed earlier. I imagine it will get easier as my body gets used to the new schedule and I figure out exactly how much I need to eat in those 8 hours.
