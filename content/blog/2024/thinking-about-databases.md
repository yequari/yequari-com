---
title: "Thinking About Databases"
date: 2024-05-08T21:31:35-07:00
year: "2024"
categories:
 - Tech
tags:
 - WeblogPoMo2024
---

Let's say you're running a web service where users can add other users as friends. How do you store each user's friends list in the database? The simplest solution would be to use one large table called "friends" with each row being an entry on one user's friends list. How well does that perform if you scale up to millions of users? Are indexes applicable to speed things up here? Is there a better way to represent this data in the database? 

I'm not super well-versed on database design so this is something I will be researching.
