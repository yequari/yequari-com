---
title: "Weekend SSD Adventures"
date: 2024-01-23T16:51:30-07:00
year: "2024"
categories:
  - Life
---

Last weekend I was really getting the semi-annual itch to play Minecraft. When I loaded it up, I realized it would be a good idea to do a backup of my world, because I hadn't done it in a while. I've poured *a lot* of hours into it with my partner and we would be devastated to lose it. So I wrote up a quick `rsync` command to send it over to my network storage (a strong term for an Raspberry Pi 4 with a USB hard disk attached). I/O error. Huh that's weird. After some intense googling, I found out my SSD has reported **535 blocks unable to be reallocated**. In other words, much of my SSD was becoming unreadable, and it was running out of good blocks to move my data to.

Turns out my SSD is a Samsung Evo 870, manufactured in early 2021. According to the Internet, a significant number of Samsung Evo 870s manufactured in early 2021 were duds, failing at absurd rates extremely early into their lifespan. It appears I ended up being one of the lucky ones, with mine lasting one and a half years before any problems became apparent. 

So I needed to get a new SSD, preferably that day, since I do need my computer to be functional. Being Sunday, all my local small computer businesses were closed, but no problem, Best Buy is open, I can just get a new one from there. Nope, the one nearest to me permanently closed. The next closest one only had the same model of SSD in stock, which wasn't ideal. I finally found one at Office Depot, of all places, on the opposite side of town. A few hours later, I had a new SSD installed in my PC, and I was ready to reinstall Linux.

This experience highlighted something I've noticed a lot recently, which is that traditional brick-and-mortar retail stores don't really carry much stock anymore. In a world where most of them got caught unprepared regarding online retail, the one advantage traditional retailers still had was that you could walk in the store, see the products you want, and buy them the same day. Without that, what is even the point of having a physical store?  
