---
title: "Dedicated Instant Messaging was Better"
date: 2024-04-23T18:00:35-07:00
year: "2024"
reply_to:
  url: https://flamedfury.com/posts/instant-messaging/
  author: flamedfury
  title: Instant Messaging
subtitle: This didn't start out as a rant about Discord, but it sure ended that way.
categories:
  - Internet
tags:
  - social media
---

fLaMEd's post about instant messaging programs got me thinking about my own IM journey. I'm too young to really have experienced the peak AIM and MSN days, but I did use MSN/Windows Live Messenger in the late 00s to keep up with some online friends I had met through various forums. Eventually, I moved to Skype as that is what all my IRL friends were on at the time. I was also on Curse Voice for the brief time it existed, mostly for voice calls during League of Legends matches. As Skype started dying out, I moved over to Discord in 2016, which made me a pretty early adopter.  

I really miss the simplicity of dedicated IM services. While Discord *is* a messaging platform, it feels more like a social network. DMs feel like a secondary feature on Discord, and I absolutely dread receiving them. I don't like that anyone, in any server we share, can DM me simply because we both exist in the same space. Or that anyone in any server we share can see what I'm up to with rich presence. I understand this is configurable, and that there have been improvements like separating DMs from strangers into "Message Requests", but my options are basically to allow everyone to message me and view my activity, or to allow no one, neither of which are what I want. On top of all this, each Discord update just makes the service worse to use as the company scrambles to find ways to monetize the platform. 

So I'm giving XMPP a try again. I've used it on and off over the past 5 years or so, and it never really stuck, since I didn't really have anyone on there to chat with. But now that I've been spending time in the indie web community, I'm finding it more useful and more fun. I spent some time setting up [ejabberd](https://www.ejabberd.im/index.html) on a VPS so that I could use one of my really dumb vanity domains for my handle. It was a pain but I got there eventually. What is appealing to me about XMPP is that 1) I get to vet the people who want to DM me, before they have messaged me, 2) Because it is separate from Discord, if I don't feel up to chatting 1-on-1, I can just close my client, and not completely close myself off from my Discord servers, and 3) Conversations are more intentional: since XMPP is purely a chat protocol, the people that use it are there to chat with others, and there are few scammers, spambots, etc. 

I'm looking forward to getting more acquainted with the protocol and chatting with some new people on there! If you want to chat on XMPP, feel free to add me via the link in the sidebar!
