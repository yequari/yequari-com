---
title: "I've Been Consumed by Honkai Star Rail"
date: 2023-06-05T00:57:08-07:00
categories: Gaming
tags:
year: "2023"
---

Normally gacha games aren't my thing, but I downloaded Honkai Star Rail on a whim and now it's consumed so much of my time. I know basically nothing about the lore, but regardless I'm really enjoying the story and characters. The combat system is really fun and I like getting to try different characters. It's so weird because I've tried to get into more "traditional" turn-based RPGs to no success but this game just scratches an itch in my brain.

Also, the idea of a space train is so silly I can't help but love it. 
