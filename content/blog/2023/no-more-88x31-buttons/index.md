---
title: "Why Are We Still Using  88x31 Buttons?"
date: 2023-10-20T17:55:51-07:00
categories:
  - Tech
  - Internet
tags:
  - smallweb
year: "2023"
---

Web 1.0 revival is pretty popular these days in the smallweb circles, but one aspect of it should have remained in the grave: the 88x31 button.
<!--more-->
![Geocities 88x31 Button](gc_icon.gif)

The origins of 88x31 button can be traced back to Geocities. The official [FTP procedures page](http://web.archive.org/web/19961220170752/http://www.geocities.com/homestead/homeftp.html) provided a code snippet that linked back to the Geocities main page and asked users to include it on their websites. The given graphic is admittedly pretty ugly, so many users ended up making custom ones that better fit the carefully crafted aesthetics of their homepage. This led to users also creating buttons of the same size for various purposes, be it for others to link back to one's site or to say "Powered by X", where X is the flavor of the month web technology that was in at the time.

Back in the 90s, the 88x31 size made sense. Most monitors were 1024x768 resolution, so screen real estate was limited, and you wanted to spend most of it on your actual site content, rather than links to other places, one of which being required. Also, every monitor was a CRT screen, which softened the appearance of the buttons and made them slightly more readable.

In today's world, most monitors and a lot of laptop screens are at a nice 1920x1080 resolution or higher, which is great for all your modern computing needs, but less great for looking at 88x31 buttons. Back in the 90s a single button would take 0.0035% of the pixels of your screen. Today that's 0.0013%, less than half of the space it took in the past. When all of these bright, colorful, and often animated buttons are stacked together in a "button wall," it can be very hard to process what each button looks like, let alone read the text. It's even worse on smartphones, where, on top of all the problems present on a desktop, it is *extremely* easy to click a different button than you meant to.

![Grandma-Finds-The-Internet](Grandma-Finds-The-Internet.jpg "What I look like trying to read your button walls.")

I get it. 88x31 buttons are a fun way to express yourself and show off what your website is about. They were an essential piece of the web 1.0 culture which many are trying to revive. And to be clear, creating fun graphics is awesome! Creating art out of nostalgia for a time gone by is rad as hell. But the 88x31 format just ain't it. It's incredibly limiting and impractical. Computer screens today have over double the screen space than those for which the 88x31 button was designed.  

All this is to say moving on from the format is long overdue. And I wouldn't spend all this time bashing on these buttons if there wasn't a modern alternative. Therefore, I propose a transition to 200x40 banners for all of our standard-size fun graphic needs! If you've been online a long time, you may be thinking to yourself, "Wait a minute, have I seen these before?" That is because you have! In the 00s, as computer screens were growing and web 2.0 was starting to take flight, many websites adopted 200x40 banners for the same purpose 88x31 buttons were used. 

The 200x40 format is is simply superior to 88x31. It's still small enough to not worry much about the filesize, but it's large enough to have text readable to my deteriorating eyes alongside a fun graphic. Stacking a bunch of them together is way less overwhelming to look at, especially if they're animated.

![yequari.com 200x40 banner](yqbanner.png "Graphic design is my passion.")

For example, look at this epic banner I threw together in 15 minutes. If this were an 88x31 button, I could have *either* the name of my website *or* the sick picture of Starscream, but not both. Could I overlay the text over the graphic? Sure, but then my choices for graphic and font color are severely limited in order to make it readable, assuming my user's eyes are good enough to even read it in the first place. With the larger banner, none of this is a problem.

This format works great for decorative banners too, as a replacement for the 88x31 buttons that convey important messages and slogans, but don't link anywhere. I've even made a few to get you started with your collection. I'm sure others can find more creative use for the expanded space than I can, but I did what I could. 

![Send me a webmention 200x40 banner](sendmeawebmention.png)
![I use arch btw 200x40 banner](iusearchbtw.png)
![Powered by Mozilla 200x40 banner](poweredbymozilla.png)
![Take Back the Web 200x40 banner](takebacktheweb.png)
![I II II IL](isthisloss.png)

To be clear, this post is only semi-serious. Ultimately, it doesn't really matter much in the grand scheme of things what size graphics we are using on our websites. It would be unreasonable of me to expect everyone to stop using these buttons. You may not agree with me that the 200x40 banner is superior, which is totally fine! After all, customizing our websites to be digital manifestations of who we are is what makes the smallweb beautiful. However, I still encourage you to consider creating one for your own site and offer multiple options for linking back to your site, so that we're not all still stuck in the 90s. 😎

*In the off chance this post does inspire you to create a 200x40 banner, please share it with me. I'd love to see what you made and add it to my site!*
