---
title: "Major Site Updates"
date: 2023-10-09T16:39:39-07:00
categories:
  - Internet
tags:
  - smallweb
year: "2023"
---

I did a complete overhaul of the layout of my site. When I launched my website a year and a half ago, I designed it while under the influence of web 1.0 nostalgia. With this redesign, my goal is to better use the amount of screen space PCs have these days, while also making it easier for the content to be responsive on mobile. 

The [links](/links) page has been revamped as well. As much as I enjoyed hiding the link descriptions until hovered, it makes for a poor experience both on mobile and for screen readers. Recent discussion about web accessibility of the smallweb in the [32-Bit cafe](https://32bit.cafe) Discord server prompted me to rethink how I wanted to present the content of my website, especially on a page as important as my outgoing links page.

I've added a [projects](/projects) page, to showcase a lot of the stuff I'm working on. I turned out to have more than I expected. I feel like I'm farting around most of the time, but in actuality I do get stuff done sometimes. 

On the backend, I've made many improvements to the maintainabilty of the site by utilizing some of the more advanced features of [Hugo](https://gohugo.io). First, my links page is now contained in a yaml file that Hugo converts into a full webpage, allowing me to easily add and organize new links. I'm also starting to get a better understanding of the templating system, allowing me to customize more pages. I'd like to write up how I did this in a post, to hopefully make Hugo more accessible to new users. 

