---
title: "Some Site Updates"
date: 2023-05-17T15:36:03-07:00
year: "2023"
categories:
  - Internet
tags:
  - smallweb
---
For the past year, this website has been generated using some Go code I wrote, which was fun while it lasted, but it got to the point where every time I wanted to add something I would have to hand-code the new feature, which resulted in my website never getting updated. So moving forward, I've gone back to using Hugo as a static site generator. It's been a great experience so far, converting my old stylesheets into a Hugo theme has been quite easy. My custom-built site generator is a project I'd like to revisit in the future, but for now ease of use takes priority.

As far as content goes, I'm working on my first "article" about some Transformers figures in my collection. I'm pretty excited about publishing it, as talking about Transformers toys was always my intention when starting this site up in the first place. In addition to that, I'm working on some minor updates to the site layout and completely revamping my home page, which all should be live relatively soon. 
