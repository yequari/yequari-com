---
title: "Re: The Art of Hyperlinking"
date: 2023-12-14T20:45:42-07:00
reply_to: 
  url: https://flamedfury.com/posts/the-art-of-hyperlinking/
  author: flamedfury
  title: The Art of Hyperlinking
categories:
  - Internet
tags: 
  - smallweb
year: "2023"
---

I recently read fLaMEd's post discussing the shortcomings of linking techniques frequently used on the smallweb, such as link pages, webrings, button walls, etc. Many websites on the smallweb employ these techniques in order to connect to other stops in the smallweb space. I don't think they are inherently bad, of course, but the context matters a lot. I want to know *why* the webmaster has chosen to put these links on their website, even if the explanation is as brief as "these people are my friends and their websites are cool." 

Frankly, links without context suck. The sheer amount of bullshit online makes it so we are all more hesitant to click links without good reason. If you like a website a lot, you should briefly explain what you like about it. Do you find it useful? Is it fun to browse? Does the webmaster share an interest of yours? With these questions unanswered, I am much less likely to click a link to another site. 

Webrings suffer this problem too. While webrings naturally provide a small amount of context in its name and topic, the sites contained within them are not always related. Devastatia observes that you see the same websites in every webring over and over, which makes them useless. If you find a new webring, why bother browsing through it if you'll see all the same sites you saw in the last webring you found? It's a shame that this is the case, as I think webrings are one of the more interesting constructs from the web 1.0 era.

It's for this reason that my site is not a part of any webrings, though I'm open to changing that if one comes along that aligns with my interests. In the meantime, my contribution to the social smallweb will be maintaining my links page with a short blurb of context for each link.
