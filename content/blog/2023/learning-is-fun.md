---
title: "Learning About Computers is Fun"
date: 2023-05-20T01:01:11-07:00
categories: Tech
tags:
year: "2023"
---
Recently, I was working on a toy Linux shell to learn about the fork and exec system calls, and I got curious about how the popular shells like bash implement shell scripting languages, which led me down a rabbithole of lexers, parsers, and formal language definitions. It led me to Robert Nystrom's [Crafting Interpreters](http://www.craftinginterpreters.com/), a book that guides you through writing an interpreter for a toy language called Lox. Nystrom's code is in Java, but I wanted to do it in C, which has been a bit of a headache. I'm really rusty in C compared to my college days, but improving my C skills and learning about programming languages has been quite an enjoyable experience, regardless. It's neat to see real applications for some of the topics covered in automata class.
