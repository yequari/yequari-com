---
title: "I Bought a Thinkpad"
date: 2023-03-22T18:09:19-07:00
categories:
    - Life
tags:
year: "2023"
---
Specifically, I got a ThinkPad T450s from 2015 for about $40. It didn’t come with storage, a power adapter, or the external battery, which cost me about an extra $100 to order. The internal battery is there but I can’t really test it until my power adapter comes in (I forgot to order it at the same time as the laptop whoops). There are also some keycaps missing. I can’t test if the switches are good yet but hopefully it turns out to be an easy fix. When I removed the back panel, I found that the plastic towards the front that the screws go into was broken, as if someone had just ripped the thing open with the screws still in there. One screw’s plastic is completely gone, but the other two are there, loosely hanging around, which is annoying, but some super glue should fix that right up. Assuming the laptop works once all my parts come in, I can’t be too mad over a $140 laptop.
