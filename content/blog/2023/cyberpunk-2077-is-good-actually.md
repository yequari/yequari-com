---
title: "Cyberpunk 2077 Is Good, Actually"
date: 2023-10-12T09:18:13-07:00
categories:
  - Gaming
tags:
year: "2023"
---

The last time I played Cyberpunk 2077 was shortly after release in late 2020 on the PS4 version, which was a [rough experience](https://www.youtube.com/watch?v=InNgPtzTpgM), to say the least. I had been excited for the game since the announcement almost ten years ago now, so the disappointment was quite painful. As a result, I left the game alone for the past 2.5 years, even though I heard the patches fixed a lot of issues. I just couldn't convince myself to try again. 

Recently the 2.0 patch dropped, followed by the Phantom Liberty DLC. I decided to give the game a go again and holy heck I've been having a blast. On current-generation systems, it looks amazing. Keanu Reeves is way less smooth. Night City feels alive and well-populated. I've encountered fewer bugs. The expressiveness of all the characters has been improved a lot. My favorite little moment so far has been a scene in the DLC where you make a call on an old landline phone. V just holds it for a second, confused. Then Johnny excitedly makes the 🤙 "call me" hand gesture, which had me rolling. 

So far I'm roughly 40 hours into the game and instead of doing the main story I keep getting distracted by all the side jobs and random events in Night City. It's a real Yakuza situation. 
