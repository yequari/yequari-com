---
title: Welcome
date: 2022-04-24T23:08:00-07:00
year: "2022"
categories:
    - Tech
---
Welcome to my new site! This project was born out of stumbling upon the Yesterweb community and my need to split off my """professional""" real-name website from my personal, hobby-oriented one. It is still very much under construction (probably perpetually) but I wanted to get something online.

This website is built in Go and exists as a single binary on my webserver, inspired by Jes Olson's post [my website is one binary](https://j3s.sh/thought/my-website-is-one-binary.html). I thought it was a super cool idea so I implemented a version myself. The code will also soon power my real-name site!

I'd also like to shout out keeri's [Flex Layouts](https://keeri.place/workshop/flex-layouts.html) tutorial providing a detailed and easy-to-understand explanation of Flexbox.
