---
title: "Self-Hosted Jellyfin"
date: "2022-05-22T23:24:06-07:00"
categories: 
    - Tech
tags:
    - self hosting
year: "2022"
---
I've been out of commission and stuck in bed for the past few weeks, and this weekend was the first time I could sit in at all in a few weeks, so I wanted to do a small project. There has been some chatter about self-hosting cloud services in the [Yesterweb forum](https://forum.yesterweb.org/viewtopic.php?p=619), and I decided I wanted to give it a shot. 

My home hardware situation is a little lacking, but I do have a Raspberry Pi 4 8GB with an external hard disk attached, which is sufficient for a few services. I decided to start with [Jellyfin](https://jellyfin.org) because I have been starting to manage my own music library locally again, to supplement my usage of Spotify. Jellyfin was attractive because it solves a long-time gripe I've had with Spotify: management of local media files is absolute shit. 

I expected setting this up to take a whole afternoon, but I was pleasantly surprised to find out it was one simple Docker command to get it running. It took a little bit of fiddling to get right for my hardware but overall was stupidly easy, to the point that this post was originally going to be a tutorial on setting it up, but a combination of the [Official Documentation](https://jellyfin.org/docs/general/administration/installing.html) and the [LinuxServer.io repo](https://github.com/linuxserver/docker-jellyfin) (for RPi-specific configuration) did a way better job of explaining it than I ever could.

After a few hours of copying files, I was good to go! The web client for listening to music is pretty good, and for my phone I use [Finamp](https://github.com/UnicornsOnLSD/finamp). 

![Image of Jellyfin homepage](/images/blog/05/220522-jellyfin.jpg)

Next I want to point a domain name at my server for easier access, and run a new Docker image for another cloud service, likely ownCloud or something similar, all behind a reverse proxy. 
