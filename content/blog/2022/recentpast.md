---
title: Missing the Recent Past
date: 2022-05-01T14:27:55-07:00
year: "2022"
categories:
    - Life
---
This year will mark two years since I graduated college. Despite graduation feeling like the resolution following the climax of the movie with the bad guy defeated and the cast living happily ever after, time continued to move forward. 
  
While in college, I studied Japanese to fulfill my degree's language requirements. In my Japanese 101 class I met a few classmates who were as weirded out as I was by the general clownery that some of the other students engaged in, and we started studying together. Over the next two semesters, we met up to study (by which I mean not doing that) almost every day and became quite close.

Today, one of them announced in our group Discord server that he was now graduating as well, and it made me think <i>damn, where did the time go?</i> as I went and looked through some of the chat logs and photos we had taken together and realized I hadn't seen any of these people for more than two years. These people were a part of my life every day for a whole year, and now I haven't seen them once in double that time. All of our circumstances have changed such that meeting up again is difficult. 

I miss that time of my life and I miss the people that had made it so special. Despite that, I don't think my feelings are of sadness, but rather I am grateful that I got to experience that year and make those memories in the first place. There is no going back to that time, but I will always have the memories.
