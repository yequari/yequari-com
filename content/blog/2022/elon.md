---
title: "Elon Musk's Twitter"
date: 2022-04-26T00:00:08-07:00
categories: 
    - Internet
tags:
    - social media
year: "2022"
---
So that's it. Twitter reached a deal with Elon Musk. There's not much to say that hasn't already been said. I want to believe Musk will make improvements, but I think whatever improvements he makes will please the wrong crowd of people. Twitter has a monopoly on the type of platform it provides, which puts him in a unique position of power, especially with all the fuss US lawmakers have been making about free speech on Twitter and elsewhere online. 

Many users have announced their departure from Twitter in response to the news. While it is great for people to get off Twitter, I doubt it will last for any meaningful amount of time. Even if it does, most of those users will just end up on a different corporate platform. I don't think there will be a mass exodus like some are predicting. That would require Musk actively making Twitter worse for the average user, and I think most of the changes he would make would affect creators more. 

We'll see how it plays out. 
