---
title: A Post About Nothing
date: 2022-04-28T22:18:26-07:00
year: "2022"
categories:
    - Life
---
My favorite thing to do is nothing. Just sitting idle, observing the environment (or more likely, my apartment wall). It's fun. I just get to think. Like David Puddy on a 16 hour flight. The world requires constant attention these days, so don't forget to take a break.
