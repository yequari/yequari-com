---
title: "I'm bad at taking breaks"
date: "2022-12-28T14:43:03-07:00"
year: "2022"
categories:
    - Life
---
I've spent the majority of my holiday break working on my PC and software instead of relaxing. Here is a quick recap of what I've been up to. 

First is powerlinx, the static site generator that runs this site. I rewrote major parts of the program and finally gave it a cli. I'm gearing up to add some cool features, like tags and categories on posts. This website now generates Atom feeds for all directories in the site contents.

I've pushed some updates to this site as well. Melon recently shared a post [Every site needs a Links Page / Why linking matters](https://melonking.net/melon.html?z=/frames/thoughts.html) that inspited me to revamp my links page. The description of each link appears when you hover your cursor over it, all done in CSS! Unfortunately this makes mobile accessibility a little worse, but I will work to make it better. I've also enabled webmentions! Combined with [brid.gy](https://brid.gy), responses from Mastodon will now show up under my post. Check out [webmentions.neocities.org](https://webmentions.neocities.org) to learn how to enable webmentions on Neocities.

I also spent some time tinkering around with my Linux desktop environment. For the past few months, my PC has been running KDE with i3wm subbed in for Kwin. I really like using i3, tiling windows and the keyboard movement feels natural, though using it alongside KDE hasn't been without problems. Iterating on a configuration for i3 and polybar as I learn what I like and what pain points I encounter has been a really fun project. 

On one hand, getting to work on interesting projects is fun but on the other I fear that I'm "wasting" my break by not relaxing "enough" and I won't be fully refreshed and ready to work. I'm probably overthinking it, trying to optimize my downtime is silly and if I'm enjoying my time that's all that should count. 
