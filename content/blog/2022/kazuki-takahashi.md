---
title: Rest in peace, Kazuki Takahashi
date: 2022-07-09T00:43:19-07:00
year: "2022"
categories:
    - Life
---
Yesterday's news about Yu-Gi-Oh! author Kazuki Takahashi really hit me hard. Yu-Gi-Oh! was a huge part of my childhood and introduced me both to card games and mange/anime. I would be a very different person without his influence. 

Thank you for everything, Takahashi-sensei. May you rest in peace.
