---
title: End of the iPod
date: 2022-05-13T23:19:33-07:00
year: "2022"
categories:
    - Life
---
This week, Apple announced it was ceasing production on the iPod touch, officially making the iPod a gadget of a bygone era.
My first iPod was a 3rd gen iPod touch, but I also owned a few Nanos.
That iPod touch was my introduction to the mobile web and app space, while smartphones were still a novelty, before they were a necessity. 
Everyone I knew had novelty apps like the beer glass and the zippo lighter. 
It was also before mobile games were basically only freemium-only, with a lot of high quality games for only $0.99. 
It's strange how different the internet and mobile tech was only 10 years ago, it certainly didn't turn out how I expected.
