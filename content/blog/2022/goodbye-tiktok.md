---
title: "I uninstalled TikTok, again"
date: 2022-11-16T16:23:00-07:00
year: "2022"
categories:
    - Internet
tags:
    - social media
---
I can't seem to find a healthy way to use TikTok, so I just shouldn't use it at all. I first installed it during the height of the pandemic in 2020 and have since uninstalled and reinstalled it at least 4 or 5 times. While there is a huge load of shit on the platform, there are also cool and interesting creators on there, lots of funny jokes, shitposts, and memes, and even videos where I legitmately learned something! However, short-form video scares me as a social media format because of how addicting it is. TikTok's algorithm in particular is so good at showing you what you want to see that you can't help but scroll. It got to the point where I was reaching for my phone and scrolling TikTok every time I had a free minute. On my worst days, this would sometimes last hours. Just scrolling. Filling up my brain with content of dubious quality and intentions. TikTok is also experiencing a rise in Internet discourse and outrage culture on the platform. Due to the short virality cycle of content on TikTok, it seems to be speedrunning the same discourse Tumblr did a decade ago. It's exhausting. I'm tired of all interaction online being subject "discourse". I'm tired of hot takes. I'm tired of hearing the unwanted opinion of every random online.

The ongoing dumpster fire formerly known as the Twitter office has also made me reconsider my social media usage in general. While it certainly is fun to watch Elongated Muskrat intentionally (there's no way this is an accident) run Twitter into the ground, it too is exhausting. There is so much going on every day with the Twitter turmoul that its too much to keep up with and stay sane, which is true of the world as a whole. There is too much happening all of the time for me to pay attention to while also maintaining my mental health, so I just won't anymore.

And so TikTok and Twitter are gone from my phone again. The next habit to kick is Reddit. 
