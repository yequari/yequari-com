var subtitles = [
    "null",
    "George likes his chicken spicy",
    "Graphic design is my passion",
    "Genesis does what Nintendon\'t",
    "1000 JS libraries in your pocket",
    "A large boulder the size of a small boulder",
    "Smooth jazz for a smooth brain",
    "Back at it again for the very first time",
    "I have no spoons and I must cope",
    "Everyone loves to barbeque, and most people are no exception",
    "The flavor of the bbq justifies the means"
];
var index = Math.floor(Math.random() * subtitles.length);
console.log("setting subtitle to " + subtitles[index]);
window.onload = function() {
    document.getElementById("subtitle").textContent = subtitles[index];
}
